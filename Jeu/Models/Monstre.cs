﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeu.Models
{   
    /// <summary>
    /// Cette classe représente toutes les différentes caractéristiques d'un monstre.
    /// </summary>
    class Monstre
    {
        public int level;
        public Bitmap imageMonstre;
        public String NomMonstre { get; set; }
        public String DescriptionMonstre { get; set; }
        public int xpDrop;
        private Random random;

        public Monstre()
        {
            this.random = new Random();
            List<MonstreDAO> listeMonstre = API.Instance.GetListeMonstresAsync().Result;
            MonstreDAO monstreDAO = listeMonstre[random.Next(listeMonstre.Count)];

            this.NomMonstre = monstreDAO.Nom;
            this.level = monstreDAO.Level;
            this.DescriptionMonstre = monstreDAO.Description;
            this.imageMonstre = new Bitmap(monstreDAO.FilePathImage);

            //set aléatoire d'xp laché par le monstre lors de sa mort
            if(level == 0)
                this.xpDrop = 50 + random.Next(50, 100);
            else
                this.xpDrop = level*100 + random.Next(level*50, level*100);
        }
    }
}
