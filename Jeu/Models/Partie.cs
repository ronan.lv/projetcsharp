﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeu.Models
{   /// <summary>
    /// Classe matérialisant les données nécessaires à une partie et le différentes actions pouvant se
    /// dérouler au cours de celle-ci.
    /// </summary>
    public class Partie
    {
        private List<Player> listeJoueurs;
        public List<Room> listeRooms;
        public List<Couloir> listeCouloirs;
        public List<int> listeHaut;
        public List<int> listeBas;
        public List<int> listeGauche;
        public List<int> listeDroite;

        public String statutPartie;
        public Player joueurEnCours;
        public int nbrDeTours;
        private Random random;

        private const int NBR_ROOMS = 15;

        public Partie(List<Player> listeJoueurs)
        {
            this.listeJoueurs = listeJoueurs;
            this.listeRooms = new List<Room>();
            this.listeCouloirs = new List<Couloir>();
            listeHaut = new List<int>();
            listeBas = new List<int>();
            listeGauche = new List<int>();
            listeDroite = new List<int>();

            this.statutPartie = "La partie va commencer";
            this.nbrDeTours = 0;
            this.random = new Random();

            this.GenererOrdreDesJoueurs();
            this.GenererListeRooms();
            this.InitialiserLesJoueurs();

            this.statutPartie = "Nouvelle partie";
            this.joueurEnCours = listeJoueurs[0];
        }

        /// <summary>
        /// initialisation des coordonées des joueurs dans la salle de départ
        /// </summary>
        private void InitialiserLesJoueurs()
        {
            for (int i = 0; i < listeJoueurs.Count; i++)
            {
                int x = listeRooms[5].Centre.X - listeRooms[5].Largeur / 4 - 8;
                int y = listeRooms[5].Centre.Y - listeRooms[5].Hauteur / 4 - 8;

                listeJoueurs[i].BrushPlayer = new SolidBrush(Color.FromArgb(random.Next(256), random.Next(256), random.Next(256)));
                switch (i)
                {
                    case (0):
                        break;
                    case (1):
                        x += listeRooms[5].Largeur / 2;
                        break;
                    case (2):
                        y += listeRooms[5].Hauteur / 2;
                        break;
                    case (3):
                        x += listeRooms[5].Largeur / 2;
                        y += listeRooms[5].Hauteur / 2;
                        break;
                }
                listeJoueurs[i].Position = new Point(x, y);
                listeJoueurs[i].RoomActuel = listeRooms[5];
                listeRooms[5].NbrJoueursIn++;
            }
        }

        /// <summary>
        /// Créer un ordre aléatoire de tour des joueurs 
        /// </summary>
        private void GenererOrdreDesJoueurs()
        {
            int random1, random2;
            Player joueurTampon = new Player();
            for (int i = 0; i < listeJoueurs.Count; i++)
            {
                random1 = random.Next(listeJoueurs.Count);
                random2 = random.Next(listeJoueurs.Count);
                joueurTampon = listeJoueurs[random1];
                listeJoueurs[random1] = listeJoueurs[random2];
                listeJoueurs[random2] = joueurTampon;
            }
        }

        /// <summary>
        /// Lis le fichier map.txt qui contient les coordonnées (point en haut à gauche et en bas à droite)
        /// de chaque salle et chaque couloir, et chaque salle possède 4 entiers, un pour chaque direction
        /// haut droite bas gauche, qui indique le numéro de la salle à laquelle elle est relié, -1 si null
        /// instancie les objets RoomTresor et RommMonstre de façon aléatoire pour chaque room
        /// </summary>
        private void GenererListeRooms()
        {
            int nbrAleatoire;
            Room room;
            List<Point> HautGaucheRoom = new List<Point>();
            List<Point> BasDroitRoom = new List<Point>();
            List<Point> HautGaucheCouloir = new List<Point>();
            List<Point> BasDroitCouloir = new List<Point>();

            String line;
            StreamReader file = new StreamReader("Resources/map.txt");
            while ((line = file.ReadLine()) != null)
            {
                if (line.Split(':')[0].Equals("room"))
                {
                    HautGaucheRoom.Add(new Point(int.Parse(line.Split(':')[2]), int.Parse(line.Split(':')[3])));
                    BasDroitRoom.Add(new Point(int.Parse(line.Split(':')[5]), int.Parse(line.Split(':')[6])));

                    int haut = int.Parse(line.Split(':')[8]);     //récupère le numéro de room auquel elle est connecté
                    int droite = int.Parse(line.Split(':')[10]);  // le numéro de room correspond à son index de la listeRoom
                    int bas = int.Parse(line.Split(':')[12]);
                    int gauche = int.Parse(line.Split(':')[14]);
                    listeHaut.Add(haut);
                    listeDroite.Add(droite);
                    listeBas.Add(bas);
                    listeGauche.Add(gauche);
                }
                else if (line.Split(':')[0].Equals("couloir"))
                {
                    HautGaucheCouloir.Add(new Point(int.Parse(line.Split(':')[2]), int.Parse(line.Split(':')[3])));
                    BasDroitCouloir.Add(new Point(int.Parse(line.Split(':')[5]), int.Parse(line.Split(':')[6])));
                }
            }
            file.Close();

            for (int i = 0; i < NBR_ROOMS; i++)
            {
                nbrAleatoire = random.Next(10);
                if (nbrAleatoire < 8)
                    room = new RoomMonstre(HautGaucheRoom[i], BasDroitRoom[i], listeHaut[i], listeBas[i], listeGauche[i], listeDroite[i]);
                else
                    room = new RoomTresor(HautGaucheRoom[i], BasDroitRoom[i], listeHaut[i], listeBas[i], listeGauche[i], listeDroite[i]);

                listeRooms.Add(room);
            }

            for (int i = 0; i < HautGaucheCouloir.Count; i++)
            {
                listeCouloirs.Add(new Couloir(HautGaucheCouloir[i], BasDroitCouloir[i]));
            }
        }

        /// <summary>
        /// permet de selectionner la salle en fonction de la direction choisie
        /// par le joueur
        /// </summary>
        /// <param name="choix"> String reçu par le client qui contient de le choix de l'user </param>
        public void FaireAvancerJoueurEnCours(String choix)
        {
            int direction = 1;
            if (choix.Equals("haut"))
            {
                direction = joueurEnCours.RoomActuel.AdjacenteHaut;
            }
            else if (choix.Equals("droite"))
            {
                direction = joueurEnCours.RoomActuel.AdjacenteDroite;
            }
            else if (choix.Equals("bas"))
            {
                direction = joueurEnCours.RoomActuel.AdjacenteBas;
            }
            else if (choix.Equals("gauche"))
            {
                direction = joueurEnCours.RoomActuel.AdjacenteGauche;
            }
            if (direction == -1)
            {
                Console.WriteLine("Direction Impossible");
                direction = 20;
            }
            joueurEnCours.AvancerNextRoom(listeRooms[direction], choix);
        }

        /// <summary>
        /// retourne un string des possibilités sous la forme HDBG
        /// exemple de possibilitées du joueur directionsPossibles:haut::bas:gauche
        /// </summary>
        /// <returns></returns>
        public String GetDirectionPossiblesJoueur()
        {
            String direction = "directionsPossibles:";
            if (joueurEnCours.RoomActuel.AdjacenteHaut != -1)
                direction += "haut";
            direction += ":";
            if (joueurEnCours.RoomActuel.AdjacenteDroite != -1)
                direction += "droite";
            direction += ":";
            if (joueurEnCours.RoomActuel.AdjacenteBas != -1)
                direction += "bas";
            direction += ":";
            if (joueurEnCours.RoomActuel.AdjacenteGauche != -1)
                direction += "gauche";
            return direction;
        }

        /// <summary>
        /// Permet de passer au tour du joueur suivant dans l'ordre de la liste
        /// </summary>
        public void PasserAuJoueurSuivant()
        {
            int indexNouveauJoueur = listeJoueurs.IndexOf(joueurEnCours) + 1;

            if (indexNouveauJoueur == listeJoueurs.Count)
            {
                indexNouveauJoueur = 0;
                nbrDeTours++;
            }

            this.joueurEnCours = listeJoueurs[indexNouveauJoueur];
        }


        /// <summary>
        /// Retourne une liste des objets joueurs dans la même 
        /// salle ou une salle adjacente au joueur en cours
        /// </summary>
        /// <returns></returns>
        public List<Player> getJoueurProche()
        {
            List<Player> listeJoueursProches = new List<Player>();
            foreach (Player p in listeJoueurs)
            {
                if (p.AdresseIP != joueurEnCours.AdresseIP)
                {
                    if (joueurEnCours.RoomActuel.AdjacenteHaut != -1)
                    {
                        if (p.RoomActuel == listeRooms[joueurEnCours.RoomActuel.AdjacenteHaut])
                            listeJoueursProches.Add(p);
                    }
                    if (joueurEnCours.RoomActuel.AdjacenteDroite != -1)
                    {
                        if (p.RoomActuel == listeRooms[joueurEnCours.RoomActuel.AdjacenteDroite])
                            listeJoueursProches.Add(p);
                    }
                    if (joueurEnCours.RoomActuel.AdjacenteBas != -1)
                    {
                        if (p.RoomActuel == listeRooms[joueurEnCours.RoomActuel.AdjacenteBas])
                            listeJoueursProches.Add(p);
                    }
                    if (joueurEnCours.RoomActuel.AdjacenteGauche != -1)
                    {
                        if (p.RoomActuel == listeRooms[joueurEnCours.RoomActuel.AdjacenteGauche])
                            listeJoueursProches.Add(p);
                    }
                    if(joueurEnCours.RoomActuel == p.RoomActuel)
                        listeJoueursProches.Add(p);
                }
            }
            return listeJoueursProches;
        }


    }
}
