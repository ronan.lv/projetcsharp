﻿using Jeu.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Threading;


namespace Jeu
{
    /// <summary>
    /// Classe contenant toutes les données d'un joueur et ses actions réalisables.
    /// </summary>
    public class Player
    {
        public String AdresseIP { get; set; }
        public String Pseudo { get; set; }
        public Boolean Disponible { get; set; }
        public Brush BrushPlayer { get; set; }
        public Point Position;
        public Room RoomActuel { get; set; }
        public JaugeXP jaugeXP;
        private Random random;
        public Boolean IsStuck { get; set; }

        private const int VITESSE_ANIMATION = 5;

        public Player()
        {
            this.jaugeXP = new JaugeXP();
            this.random = new Random();
            this.IsStuck = false;
        }

        /// <summary>
        /// Permet de sommer les mouvements d'un joueur
        /// </summary>
        /// <param name="roomSuivante"></param>
        /// <param name="direction"></param>
        public void AvancerNextRoom(Room roomSuivante, String direction)
        {
            this.ExitRoom();
            this.RoomActuel.NbrJoueursIn--;
            this.AvancerDansLeCouloir(roomSuivante, direction);
            this.EnterRoom(roomSuivante);
            this.RoomActuel = roomSuivante;
            this.RoomActuel.NbrJoueursIn++;
        }

        /// <summary>
        /// permet d'ajouter l'expérience obtenu à celle du joueur
        /// </summary>
        /// <param name="XP"></param>
        public void RecolterXP(int XP)
        {
            this.jaugeXP.addXP(XP);
        }

        /// <summary>
        /// calcule des chances de victoire du combat en fonction du nombre d'alliées, 
        /// du nombre d'ennemis et le niveau du monstre
        /// </summary>
        /// <param name="listAllies"></param>
        /// <param name="listEnnemies"></param>
        /// <returns>l'xp gagné à l'issu du vombat, 0 si c'est une défaitre</returns>
        public int Combattre(List<Player> listAllies, List<Player> listEnnemies)
        {
            int niveauCombines;
            int probabiliteVictoire = 0;
            int xp;

            niveauCombines = this.jaugeXP.LevelJauge + listAllies.Count - listEnnemies.Count;

            if (niveauCombines > ((RoomMonstre)this.RoomActuel).monstre.level)
                probabiliteVictoire = 100;
            if (niveauCombines == (((RoomMonstre)this.RoomActuel).monstre.level + 0))
                probabiliteVictoire = 60;
            if (niveauCombines == (((RoomMonstre)this.RoomActuel).monstre.level - 1))
                probabiliteVictoire = 50;
            if (niveauCombines == (((RoomMonstre)this.RoomActuel).monstre.level - 2))
                probabiliteVictoire = 30;
            if (niveauCombines == (((RoomMonstre)this.RoomActuel).monstre.level - 3))
                probabiliteVictoire = 10;


            xp = ((RoomMonstre)this.RoomActuel).monstre.xpDrop;
            if (random.Next(100) < probabiliteVictoire)
                return xp;
            else
                return 0;
        }

        /// <summary>
        /// permet au joueur d'avancer en fonction d'une direction juqu'au centre de la salle suivante
        /// </summary>
        /// <param name="roomSuivante"></param>
        /// <param name="direction"></param>
        private void AvancerDansLeCouloir(Room roomSuivante, String direction)
        {
            switch(direction)
            {
                case ("haut"):
                    while (this.Position.Y > roomSuivante.Centre.Y - 8)   // déplacement vers le haut
                    {
                        this.Position.Y -= 1;
                        Thread.Sleep(VITESSE_ANIMATION);
                    }
                    break;

                case ("droite"):
                    while (this.Position.X < roomSuivante.Centre.X - 8)   // déplacement vers la droite
                    {
                        this.Position.X += 1;
                        Thread.Sleep(VITESSE_ANIMATION);
                    }
                    break;

                case ("gauche"):
                    while (this.Position.X > roomSuivante.Centre.X - 8)   // déplacement vers la gauche
                    {
                        this.Position.X -= 1;
                        Thread.Sleep(VITESSE_ANIMATION);
                    }
                    break;

                case ("bas"):
                    while (this.Position.Y < roomSuivante.Centre.Y - 8)   // déplacement vers le bas
                    {
                        this.Position.Y += 1;
                        Thread.Sleep(VITESSE_ANIMATION);
                    }
                    break;
                default:
                    Debug.WriteLine("Aucun déplacement, erreur direction");
                    break;
            }

        }

        /// <summary>
        /// permet au joueur de se placer au centre de la salle de laquelle il veut sortir
        /// </summary>
        private void ExitRoom()
        {
            if (Position.Y < RoomActuel.Centre.Y - 8)                //si l'ordonnée du joueur est inférieur au centre
            {
                while (Position.Y != RoomActuel.Centre.Y - 8)
                {
                    Position.Y += 1;
                    Thread.Sleep(VITESSE_ANIMATION);
                }
            }
            else if (Position.Y > RoomActuel.Centre.Y - 8)         //si l'ordonnée du joueur est supérieur au centre
            {
                while (Position.Y != RoomActuel.Centre.Y - 8)
                {
                    Position.Y -= 1;
                    Thread.Sleep(VITESSE_ANIMATION);
                }
            }
            if (Position.X < RoomActuel.Centre.X - 8)                //si l'abscisse du joueur est inférieur au centre
            {
                while (Position.X != RoomActuel.Centre.X - 8)
                {
                    Position.X += 1;
                    Thread.Sleep(VITESSE_ANIMATION);
                }
            }
            else if (Position.X > RoomActuel.Centre.X - 8)           //si l'abscisse du joueur est supérieur au centre
            {
                while (Position.X != RoomActuel.Centre.X - 8)
                {
                    Position.X -= 1;
                    Thread.Sleep(VITESSE_ANIMATION);
                }
            }
        }

        /// <summary>
        /// Permet au joueur de se placer dans la salle en fct du nombre de joueurs déjà présent
        /// calcule les place avec les quarts de coordonnées de la salle
        /// </summary>
        /// <param name="roomSuivante"> Salle dans laquelle on souhaite entrer</param>
        private void EnterRoom(Room roomSuivante)
        {
            //organisation des places dans la salle
            /*     | 0   1 |
             *     | 2   3 |
             */
            switch (roomSuivante.NbrJoueursIn)      //nbrDeJoueursDéjaPrésentDans LaSalle
            {
                case 0:
                    while (this.Position.X > roomSuivante.Centre.X - (roomSuivante.Largeur / 4) - 8)
                    {
                        this.Position.X -= 1;
                        Thread.Sleep(VITESSE_ANIMATION);
                    }
                    while (this.Position.Y > roomSuivante.Centre.Y - (roomSuivante.Hauteur / 4) - 8)
                    {
                        this.Position.Y -= 1;
                        Thread.Sleep(VITESSE_ANIMATION);
                    }
                    break;

                case 1:
                    while (this.Position.X < roomSuivante.Centre.X + (roomSuivante.Largeur / 4) - 8)
                    {
                        this.Position.X += 1;
                        Thread.Sleep(VITESSE_ANIMATION);
                    }
                    while (this.Position.Y > roomSuivante.Centre.Y - (roomSuivante.Hauteur / 4) - 8)
                    {
                        this.Position.Y -= 1;
                        Thread.Sleep(VITESSE_ANIMATION);
                    }
                    break;

                case 2:
                    while (this.Position.X > roomSuivante.Centre.X - (roomSuivante.Largeur / 4) - 8)
                    {
                        this.Position.X -= 1;
                        Thread.Sleep(VITESSE_ANIMATION);
                    }
                    while (this.Position.Y < roomSuivante.Centre.Y + (roomSuivante.Hauteur / 4) - 8)
                    {
                        this.Position.Y += 1;
                        Thread.Sleep(VITESSE_ANIMATION);
                    }
                    break;

                case 3:
                    while (this.Position.X < roomSuivante.Centre.X + (roomSuivante.Largeur / 4) - 8)
                    {
                        this.Position.X += 1;
                        Thread.Sleep(VITESSE_ANIMATION);
                    }
                    while (this.Position.Y < roomSuivante.Centre.Y + (roomSuivante.Hauteur / 4) - 8)
                    {
                        this.Position.Y += 1;
                        Thread.Sleep(VITESSE_ANIMATION);
                    }
                    break;

                default:
                    while (this.Position.X < roomSuivante.Centre.X + (roomSuivante.Largeur / 4) - 8)
                    {
                        this.Position.X += 1;
                        Thread.Sleep(VITESSE_ANIMATION);
                    }
                    while (this.Position.Y < roomSuivante.Centre.Y + (roomSuivante.Hauteur / 4) - 8)
                    {
                        this.Position.Y += 1;
                        Thread.Sleep(VITESSE_ANIMATION);
                    }
                    Console.WriteLine("Erreur choix place");
                    break;
            }

        }


    }
}
