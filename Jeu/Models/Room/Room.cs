﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeu
{
    public class Room
    {

        public Point PointEnHautAGauche { get; set; }
        public Point PointEnBasADroite { get; set; }
        public Point Centre;
        public int Largeur { get; set; }
        public int Hauteur { get; set; }
        public int NbrJoueursIn { get; set; }
        public int AdjacenteHaut, AdjacenteBas, AdjacenteGauche, AdjacenteDroite;
        protected Random random;

        public Room(Point hautGauche, Point basDroite, int AdjacenteHaut, int AdjacenteBas, int AdjacenteGauche, int AdjacenteDroite)
        {
            this.AdjacenteBas = AdjacenteBas;
            this.AdjacenteHaut = AdjacenteHaut;
            this.AdjacenteDroite = AdjacenteDroite;
            this.AdjacenteGauche = AdjacenteGauche;

            this.PointEnHautAGauche = hautGauche;
            this.PointEnBasADroite = basDroite;
            this.Largeur = basDroite.X - hautGauche.X;
            this.Hauteur = basDroite.Y - hautGauche.Y;
            this.Centre = new Point(hautGauche.X + (Largeur / 2), hautGauche.Y + (Hauteur / 2));
            this.NbrJoueursIn = 0;
            this.random = new Random();
        }

    }
}
