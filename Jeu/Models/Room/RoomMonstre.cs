﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeu.Models
{
    class RoomMonstre : Room
    {
        public Monstre monstre;
        public Boolean DisplayMonstre { get; set; }
        public Boolean DisplayCarteMonstre { get; set; }


        public RoomMonstre(Point hautGauche, Point basDroite, int Haut, int Bas, int Gauche, int Droite) 
            : base(hautGauche, basDroite, Haut, Bas, Gauche, Droite)
        {
            this.GenererUnNouveauMonstre();
            DisplayMonstre = false;
            DisplayCarteMonstre = false;
        }

        public void  GenererUnNouveauMonstre()
        {
            monstre = new Monstre();
        }

    }
}
