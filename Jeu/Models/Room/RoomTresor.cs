﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeu.Models
{
    class RoomTresor : Room
    {
        public int XPTresor;

        public RoomTresor(Point hautGauche, Point basDroite, int Haut, int Bas, int Gauche, int Droite) 
            : base(hautGauche, basDroite, Haut, Bas, Gauche, Droite)
        {
            int lvl = this.random.Next(4);
            this.XPTresor = random.Next(100, 800);
        }
    }
}
