﻿using Jeu.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Jeu
{   /// <summary>
    /// Cette classe contient les méthodes nécessaires pour communiquer avec un client. 
    /// Les métodes d'écoutes de demande de connexions et d'écoute de messages clients sont non-bloquantes.
    /// </summary>
    public class CommunicationServeur
    {
        private Socket socketServeur;
        private List<Socket> listeClients;

        public CommunicationServeur()
        {
            socketServeur = new Socket(SocketType.Stream, ProtocolType.Tcp);
            socketServeur.Blocking = false;
            listeClients = new List<Socket>();
        }

        public void DemarrerServeur()
        {
            try
            {
                // A Socket must be associated with an endpoint using the Bind method  
                socketServeur.Bind(new IPEndPoint(IPAddress.Parse(ADRESSEIP.myIpLocal), 11000));
                // Specify how many requests a Socket can listen before it gives Server busy response.  
                socketServeur.Listen(4);
            }
            catch (Exception e)
            {
                Debug.WriteLine("Erreur dans CommunicationServeur : " + e.ToString());
            }
        }

        public void DeconnecterClient(String ipJoueur)
        {
            try
            {
                foreach (Socket socketClient in listeClients)
                {
                    if (((IPEndPoint)socketClient.RemoteEndPoint).Address.MapToIPv4().ToString().Equals(ipJoueur))
                    {
                        socketClient.Shutdown(SocketShutdown.Both);
                        socketClient.Close();
                        listeClients.Remove(socketClient);
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("Erreur dans CommunicationServeur : " + e.ToString());
            }
        }

        public void AccepterConnexionNouveauClient(ref String ipNouveauJoueur)
        {
            try
            {
                listeClients.Add(socketServeur.Accept());
                EndPoint endPointClient = listeClients[listeClients.Count - 1].RemoteEndPoint;
                ipNouveauJoueur = ((IPEndPoint)endPointClient).Address.MapToIPv4().ToString();
            }
            catch (Exception e)
            {
                Debug.WriteLine("Erreur dans CommunicationServeur : " + e.ToString());
            }
        }

        public void AfficherTousLesClientsConnectes()
        {
            if (listeClients.Count != 0)
            {
                foreach (Socket socketClient in listeClients)
                {
                    Console.WriteLine(socketClient.RemoteEndPoint);
                }
            }
            else
            {
                Console.WriteLine("Il n'y a aucun client connecté !");
            }
        }

        /// <summary>
        /// permet d'envoyer un msg string à un client précis par son ip
        /// </summary>
        /// <param name="message"></param>
        /// <param name="ipJoueur"></param>
        public void EnvoyerUnMessage(String message, String ipJoueur)
        {

            try
            {
                foreach (Socket socketClient in listeClients)
                {
                    if (((IPEndPoint)socketClient.RemoteEndPoint).Address.MapToIPv4().ToString().Equals(ipJoueur))
                    {
                        byte[] messageEnBinaire = Encoding.ASCII.GetBytes(message);
                        socketClient.Send(messageEnBinaire);
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("Erreur dans CommunicationServeur : " + e.ToString());
            }
        }

        /// <summary>
        /// permet de recevoir un msg d'un client précis par son IP
        /// </summary>
        /// <param name="ipJoueur"></param>
        /// <returns></returns>
        public String EcouterMessage(String ipJoueur)
        {
            byte[] msgRecuEnBinaire = new byte[1024];
            try
            {
                foreach (Socket socketClient in listeClients)
                {

                    if (((IPEndPoint)socketClient.RemoteEndPoint).Address.MapToIPv4().ToString().Equals(ipJoueur))
                    {
                        int nbrOctetsRecus = socketClient.Receive(msgRecuEnBinaire);
                        return Encoding.ASCII.GetString(msgRecuEnBinaire, 0, nbrOctetsRecus);
                    }

                }
                return null;
            }
            catch (Exception e)
            {
                Debug.WriteLine("Erreur dans CommunicationServeur : " + e.ToString());
                return null;
            }

        }
    }
}
