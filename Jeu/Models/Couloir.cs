﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeu.Models
{   /// <summary>
    /// Clase permettant de représenter les données inhérentes à un couloir.
    /// </summary>
    public class Couloir
    {
        public Point PointEnHautAGauche { get; set; }
        public Point PointEnBasADroite { get; set; }
        public Point Centre;
        public int Largeur { get; set; }
        public int Hauteur { get; set; }

        public Couloir(Point hautGauche, Point basDroite)
        {
            this.PointEnHautAGauche = hautGauche;
            this.PointEnBasADroite = basDroite;
            this.Largeur = basDroite.X - hautGauche.X;
            this.Hauteur = basDroite.Y - hautGauche.Y;
            this.Centre = new Point(hautGauche.X + (Largeur / 2), hautGauche.Y + (Hauteur / 2));
        }
        
    }
}
