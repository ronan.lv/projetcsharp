﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Jeu.Models
{   /// <summary>
    /// Cette classe représente les différents comportements et données de la jauge d'XP d'un joueur. Elle permet
    /// notamment de faire le lien entre son xp total et son niveau actuel ainsi que l'xp lui restant à acquérir pour passer
    /// au niveau suivant.
    /// </summary>
    public class JaugeXP
    {
        public int LevelJauge { get; set; }
        public int XPTotal { get; set; }
        public int XPJauge { get; set; }
        public double ratioJauge {get; set;}

        public JaugeXP()
        {
            this.LevelJauge = 0;
            this.XPTotal = 0;
            this.XPJauge = 0;
            this.ratioJauge = 0;
        }

        /*Level 1 -> 100 XP (XPtotal = 100)
        * Level 2 -> 200 Xp (XPtotal = 300)
        * Level 3 -> 400 Xp (XPtotal = 700)
        * Level 4 -> 800 Xp (XPtotal = 1500)
        * Level 5 -> 1600 Xp (XPtotal = 3100)
        */
        public void addXP(int XP)
        {   
            this.XPTotal += XP;
            if(XPTotal < 100)
            {
                this.LevelJauge = 0;
                this.XPJauge = XPTotal;
                this.ratioJauge = ((double) this.XPJauge) / 100;
            }
            else if(XPTotal < 300)
            {
                this.LevelJauge = 1;
                this.XPJauge = XPTotal - 100;
                this.ratioJauge = ((double) this.XPJauge) / 200;
            }
            else if(XPTotal < 700)
            {
                this.LevelJauge = 2;
                this.XPJauge = XPTotal - 100 - 200;
                this.ratioJauge = ((double) this.XPJauge) / 400;
            }
            else if (XPTotal < 1500)
            {
                this.LevelJauge = 3;
                this.XPJauge = XPTotal - 100 - 200 - 400;
                this.ratioJauge = ((double) this.XPJauge) / 800;
            }
            else if (XPTotal < 3100)
            {
                this.LevelJauge = 4;
                this.XPJauge = XPTotal - 100 - 200 - 400 - 800;
                this.ratioJauge = ((double) this.XPJauge) / 1600;
            }
        }

    }
}
