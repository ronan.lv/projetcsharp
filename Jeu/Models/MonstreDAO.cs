﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jeu
{   
    /// <summary>
    /// Cette classe est une représentation simplifiée d'un monstre. Elle correspond en fait à l'objet monstre tel qu'il
    /// est stocké en BDD.
    /// </summary>
    public class MonstreDAO
    {   
        public long Id { get; set; }
        public string Nom { get; set; }
        public int Level { get; set; }
        public string FilePathImage { get; set; }
        public string Description { get; set; }

    }
}
