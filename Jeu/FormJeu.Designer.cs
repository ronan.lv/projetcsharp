﻿namespace Jeu
{
    partial class FormJeu
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing); 
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timerMove = new System.Windows.Forms.Timer(this.components);
            this.textDescription = new System.Windows.Forms.TextBox();
            this.affichageCarteMonstreNom = new System.Windows.Forms.TextBox();
            this.affichageCarteMonstreDescription = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // timerMove
            // 
            this.timerMove.Enabled = true;
            this.timerMove.Interval = 10;
            this.timerMove.Tick += new System.EventHandler(this.timerMove_Tick);
            // 
            // textDescription
            // 
            this.textDescription.BackColor = System.Drawing.Color.White;
            this.textDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textDescription.Location = new System.Drawing.Point(35, 1100);
            this.textDescription.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textDescription.Multiline = true;
            this.textDescription.Name = "textDescription";
            this.textDescription.ReadOnly = true;
            this.textDescription.Size = new System.Drawing.Size(530, 90);
            this.textDescription.TabIndex = 0;
            this.textDescription.TabStop = false;
            // 
            // affichageCarteMonstreNom
            // 
            this.affichageCarteMonstreNom.BackColor = System.Drawing.Color.LightSalmon;
            this.affichageCarteMonstreNom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.affichageCarteMonstreNom.Location = new System.Drawing.Point(590, 1100);
            this.affichageCarteMonstreNom.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.affichageCarteMonstreNom.Multiline = true;
            this.affichageCarteMonstreNom.Name = "affichageCarteMonstreNom";
            this.affichageCarteMonstreNom.ReadOnly = true;
            this.affichageCarteMonstreNom.Size = new System.Drawing.Size(600, 30);
            this.affichageCarteMonstreNom.TabIndex = 1;
            this.affichageCarteMonstreNom.TabStop = false;
            // 
            // affichageCarteMonstreDescription
            // 
            this.affichageCarteMonstreDescription.BackColor = System.Drawing.Color.LightSalmon;
            this.affichageCarteMonstreDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.affichageCarteMonstreDescription.Location = new System.Drawing.Point(590, 1130);
            this.affichageCarteMonstreDescription.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.affichageCarteMonstreDescription.Multiline = true;
            this.affichageCarteMonstreDescription.Name = "affichageCarteMonstreDescription";
            this.affichageCarteMonstreDescription.ReadOnly = true;
            this.affichageCarteMonstreDescription.Size = new System.Drawing.Size(600, 60);
            this.affichageCarteMonstreDescription.TabIndex = 2;
            this.affichageCarteMonstreDescription.TabStop = false;
            // 
            // FormJeu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(1717, 1055);
            this.Controls.Add(this.affichageCarteMonstreDescription);
            this.Controls.Add(this.affichageCarteMonstreNom);
            this.Controls.Add(this.textDescription);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FormJeu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Jeu";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.MoteurGraphique);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Timer timerMove;
        private System.Windows.Forms.TextBox textDescription;
        private System.Windows.Forms.TextBox affichageCarteMonstreNom;
        private System.Windows.Forms.TextBox affichageCarteMonstreDescription;
    }
}

