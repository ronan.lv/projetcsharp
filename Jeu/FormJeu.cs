﻿using Jeu.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace Jeu
{   /// <summary>
    /// Cette classe est le controlleur de la vue (vue correspondant à l'écran de jeu) du même nom. 
    /// Elle comporte à la fois le moteur graphique chargé de redessiner la fenêtre à intervalles constant et
    /// le moteur de jeu, qui se charge d'appeller les méthodes nécessaires de l'objet Partie et de faire le lien avec
    /// la communciation réseau via l'objet CommunicationServeur. Le moteur de Jeu tourne dans un Thread séparé.
    /// </summary>
    public partial class FormJeu : Form
    {
        private List<Player> listeJoueurs;
        private CommunicationServeur communicationServeur;
        private Partie partie;
        private List<Player> listeJoueursProches;

        private const int TEMPO = 1500;

        public FormJeu(CommunicationServeur communicationServeur, List<Player> listeJoueurs)
        {
            this.communicationServeur = communicationServeur;
            this.listeJoueurs = listeJoueurs;

            this.partie = new Partie(listeJoueurs);

            InitializeComponent();
            this.SetStyle(                          //prevent flickering/blinking
                ControlStyles.UserPaint |
                ControlStyles.AllPaintingInWmPaint |
                ControlStyles.OptimizedDoubleBuffer,
                true);

            Thread threadControlleurDeJeu = new Thread(MoteurDeJeu);
            threadControlleurDeJeu.Start();
        }

        private void MoteurDeJeu()
        {
            int resultatCombat;
            string messageRecu;
            string messageDirection;
            string messageAllie;
            List<Player> listeAllie = new List<Player>();
            List<Player> listeEnnemie = new List<Player>();
            List<Player> listeNeutre = new List<Player>();
            Thread.Sleep(1000); //Permer à la Form de se charger correctement


            while (1 == 1)
            {
                // début du tour
                partie.statutPartie = "Tour de " + partie.joueurEnCours.Pseudo;
                Thread.Sleep(TEMPO);
                if (!partie.joueurEnCours.IsStuck)
                {
                    // si  le joueur n'est pas bloqué, on demande à l'utilisateur où il souhaite aller
                    partie.statutPartie = partie.joueurEnCours.Pseudo + " où voulez-vous allez ?";
                    communicationServeur.EnvoyerUnMessage(partie.GetDirectionPossiblesJoueur(), partie.joueurEnCours.AdresseIP);
                    messageDirection = this.EcouterMessageBloquant(partie.joueurEnCours.AdresseIP, "choixDirection");
                    partie.FaireAvancerJoueurEnCours(messageDirection);
                }
                else
                {
                    partie.statutPartie = partie.joueurEnCours.Pseudo + " est bloqué dans cette salle";
                    Thread.Sleep(TEMPO);
                }
                if (partie.joueurEnCours.RoomActuel is RoomMonstre)
                {
                    //le joueur est face à un monstre
                    RoomMonstre roomMonstre = (RoomMonstre)partie.joueurEnCours.RoomActuel;
                    partie.statutPartie = partie.joueurEnCours.Pseudo + " est face à un Monstre !";
                    Thread.Sleep(TEMPO);
                    partie.statutPartie = "Un monstre de lvl " + roomMonstre.monstre.level + " apparaît.";
                    roomMonstre.DisplayMonstre = true;
                    roomMonstre.DisplayCarteMonstre = true;
                    Thread.Sleep(TEMPO);

                    communicationServeur.EnvoyerUnMessage("demandeChoixJoueur:combattreOuFuir", partie.joueurEnCours.AdresseIP);
                    partie.statutPartie = partie.joueurEnCours.Pseudo + " que voulez vous faire ?";
                    messageRecu = this.EcouterMessageBloquant(partie.joueurEnCours.AdresseIP, "choixEntreeSalle");
                    if (messageRecu.Equals("combattre"))
                    {
                        //le joueur choisit de combattre
                        partie.statutPartie = partie.joueurEnCours.Pseudo + " décide d'affronter le Monstre.";
                        Thread.Sleep(TEMPO);

                        listeJoueursProches = partie.getJoueurProche();
                        if (listeJoueursProches.Count != 0)
                        {
                            //on demande aux joueurs adjacents/proches, leur décision, aider trahir ou ne rien faire
                            partie.statutPartie = "Les autres, que voulez vous faire ?";
                            foreach (Player p in listeJoueursProches)
                            {
                                if (p.AdresseIP != partie.joueurEnCours.AdresseIP)
                                    communicationServeur.EnvoyerUnMessage("demandeChoixJoueur:aiderOuTrahirOuNeRienFaire", p.AdresseIP);
                            }
                            //on récupère le choix des joueurs et on les ajoute à la liste correspondante
                            while ((listeAllie.Count + listeEnnemie.Count + listeNeutre.Count) < listeJoueurs.Count - 1)
                            {
                                foreach (Player joueurProche in listeJoueursProches)
                                {
                                    if (joueurProche.AdresseIP != partie.joueurEnCours.AdresseIP)
                                    {
                                        messageAllie = communicationServeur.EcouterMessage(joueurProche.AdresseIP);
                                        if (messageAllie != null)
                                        {
                                            if (messageAllie.Contains("aider"))
                                            {
                                                listeAllie.Add(joueurProche);
                                                partie.statutPartie = joueurProche.Pseudo + " décide d'aider " + partie.joueurEnCours.Pseudo + " à affronter le Monstre.";
                                            }
                                            else if (messageAllie.Contains("trahir"))
                                            {
                                                listeEnnemie.Add(joueurProche);
                                                partie.statutPartie = joueurProche.Pseudo + " décide de trahir " + partie.joueurEnCours.Pseudo + " ! ";
                                            }
                                            else if (messageAllie.Contains("neRienFaire"))
                                            {
                                                listeNeutre.Add(joueurProche);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        Thread.Sleep(TEMPO);
                        //Calcul du combat
                        resultatCombat = partie.joueurEnCours.Combattre(listeAllie, listeEnnemie);
                        listeAllie.Clear();
                        listeEnnemie.Clear();
                        listeNeutre.Clear();
                        //selon l'issue du combat, soit on affiche une victoire ou une defaite
                        if (resultatCombat != 0)
                        {
                            partie.statutPartie = partie.joueurEnCours.Pseudo + " a vaincu le monstre et a gagné " + resultatCombat + " xp !";
                            communicationServeur.EnvoyerUnMessage("xpGagne:" + resultatCombat, partie.joueurEnCours.AdresseIP);
                            partie.joueurEnCours.RecolterXP(resultatCombat);
                            partie.joueurEnCours.IsStuck = false;
                            Thread.Sleep(TEMPO);
                        }
                        else
                        {
                            partie.statutPartie = partie.joueurEnCours.Pseudo + " n'est pas parvenu a vaincre le monstre";
                            partie.joueurEnCours.IsStuck = true;
                            Thread.Sleep(TEMPO);
                        }
                    }
                    else if (messageRecu.Equals("fuir"))
                    {
                        //si le joueur décide d'éviter le combat
                        partie.statutPartie = partie.joueurEnCours.Pseudo + " décide de prendre la fuite.";
                        partie.joueurEnCours.IsStuck = false;
                        Thread.Sleep(TEMPO);
                    }
                    roomMonstre.DisplayMonstre = false;
                    roomMonstre.DisplayCarteMonstre = false;
                }
                else if (partie.joueurEnCours.RoomActuel is RoomTresor)
                {
                    //si le joueur rencontre un trésor
                    RoomTresor roomTresor = ((RoomTresor)partie.joueurEnCours.RoomActuel);
                    if (roomTresor.XPTresor != 0)
                    {
                        partie.statutPartie = partie.joueurEnCours.Pseudo + " a découvert un trésor légendaire !";
                        Thread.Sleep(1000);
                        partie.joueurEnCours.RecolterXP(roomTresor.XPTresor);
                        partie.statutPartie = partie.joueurEnCours.Pseudo + " a gagné " + roomTresor.XPTresor + " xp !";
                        communicationServeur.EnvoyerUnMessage("xpGagne:" + roomTresor.XPTresor, partie.joueurEnCours.AdresseIP);
                        roomTresor.XPTresor = 0;
                    }
                    else
                    {
                        partie.statutPartie = "Cette salle a déja été pillée...";
                    }

                    Thread.Sleep(TEMPO);
                }

                partie.PasserAuJoueurSuivant();
            }

        }

        /// <summary>
        /// permet de bloquer le jeu tant qu'on ne reçoit pas une réponse de cette adressse ip
        /// </summary>
        /// <param name="adresseIP"></param>
        /// <param name="typeMessage"></param>
        /// <returns></returns>
        private String EcouterMessageBloquant(String adresseIP, String typeMessage)
        {
            Boolean messageOK = false;
            String messageRecu = null;
            while (!messageOK)
            {
                messageRecu = communicationServeur.EcouterMessage(adresseIP);
                if (messageRecu != null)
                {
                    if (messageRecu.Contains(typeMessage))
                        messageOK = true;
                }
            }
            return messageRecu.Substring(messageRecu.IndexOf(":") + 1);
        }

        /// <summary>
        /// affichage des éléments, salles, couloirs, personnages, monstre, zone de texte
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MoteurGraphique(object sender, PaintEventArgs e)
        {
            foreach (Room r in partie.listeRooms)
                e.Graphics.FillRectangle(Brushes.White, new Rectangle(r.PointEnHautAGauche.X, r.PointEnHautAGauche.Y, r.Largeur, r.Hauteur));

            foreach (Couloir c in partie.listeCouloirs)
                e.Graphics.FillRectangle(Brushes.White, new Rectangle(c.PointEnHautAGauche.X, c.PointEnHautAGauche.Y, c.Largeur, c.Hauteur));

            foreach (Player joueur in listeJoueurs)
            {
                e.Graphics.DrawString(
                    joueur.Pseudo + "\nlvl " + joueur.jaugeXP.LevelJauge,
                    new Font("Arial", 8),
                    new SolidBrush(Color.Black),
                    joueur.Position.X - (joueur.Pseudo.Length / 2),
                    joueur.Position.Y - 20);

                e.Graphics.FillRectangle(joueur.BrushPlayer, new Rectangle(joueur.Position.X, joueur.Position.Y + 7, 20, 20));
            }

            if (partie.joueurEnCours.RoomActuel is RoomMonstre)
            {
                RoomMonstre r = (RoomMonstre)partie.joueurEnCours.RoomActuel;
                if (r.DisplayMonstre == true)
                    e.Graphics.DrawImage(r.monstre.imageMonstre, r.Centre.X - 16, r.Centre.Y - 16, 40, 40);
                if (r.DisplayCarteMonstre == true)
                {
                    this.affichageCarteMonstreNom.Text = r.monstre.NomMonstre + " - lvl " + r.monstre.level;
                    this.affichageCarteMonstreDescription.Text = r.monstre.DescriptionMonstre;
                    this.affichageCarteMonstreNom.Visible = true;
                    this.affichageCarteMonstreDescription.Visible = true;
                }
                else
                {
                    this.affichageCarteMonstreNom.Visible = false;
                    this.affichageCarteMonstreDescription.Visible = false;
                }

            }
            textDescription.Text = partie.statutPartie;
        }

        private void timerMove_Tick(object sender, EventArgs e)
        {
            Invalidate();
        }

    }
}
