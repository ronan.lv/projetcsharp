﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;
using Timer = System.Windows.Forms.Timer;

namespace Jeu
{   /// <summary>
    /// Cette classe est un controlleur de la vue homonyme.
    /// Elle permet également de recevoir les demandes de connexions des joueurs puis de valider ou non celle-ci en vérifiant
    /// que le code envoyé par le joueur correspond bien à celui demandé par l'écran de jeu.
    /// Les interactions réseaux sont réalisées dans une boucle (car méthodes non bloquantes), elle-même placée dans un Thread
    /// afin d'éviter un freeze / crash de l'affichage.
    /// Etant donnée que la gestion de l'affichage est sécurisée par WinForm, on ne peut modifier les composants graphiques que dans le Thread 
    /// principale. C'est pour cela que l'on utilise des delegates ainsi que la méthode Invoke() pour réaliser des modifaications graphiques
    /// dans notre Thread réseau.
    /// </summary>
    public partial class FormConnexion : Form
    {
        private Boolean tousLesJoueursSontPrets;

        private String codePartie;
        private String ipNouveauJoueur = null;
        private String messageRecu = null;
        private Boolean lancementPartie;
        private List<Player> listeJoueurs;
        private CommunicationServeur communicationServeur;
        private Timer timer;
        private int cptTicksTimer;
        private Random random;

        private Thread threadTest;

        public delegate void DelegateAdd(String ipNouveauJoueur);
        public delegate void DelegateRefresh(Player joueur);
        public delegate void DelegateDecompte();
        public delegate void DelegateAfficherCodePartie();
        public FormConnexion(CommunicationServeur communicationServeur, List<Player> listeJoueurs)
        {
            InitializeComponent();
            this.random = new Random();
            this.codePartie = random.Next(1000,9999).ToString();
            this.communicationServeur = communicationServeur;
            this.tousLesJoueursSontPrets = false;
            this.listeJoueurs = listeJoueurs;
            this.lancementPartie = false;
            this.cptTicksTimer = 0;
            this.timer = new Timer(); 
            
            communicationServeur.DemarrerServeur();

            threadTest = new Thread(GererLesConnexions);
            threadTest.Start();
        }




        /**
         * Boucle qui détecte et ajpoute les user avec adresse ip, pseudo et si l'user est pret ou non
         * sort de la boucle quand le nombre max de joueur est atteint 
         */
        private void GererLesConnexions()
        {
            Thread.Sleep(500);
            Invoke((DelegateAfficherCodePartie) AfficherCodePartie);
            tousLesJoueursSontPrets = false;
            int nbrJoueursPrets = 0;
            String disponibiliteJoueur = null;

            while(!lancementPartie)
            {


                /** 
                 * Cette partie ce charge de vérifier si il y des joueurs essayant de se connecter, puis le cas échant,
                 * reçoit leur code de partie et vérifie qu'il correspond bien a celui demandé
                 */
                if (ipNouveauJoueur == null)
                {
                    communicationServeur.AccepterConnexionNouveauClient(ref ipNouveauJoueur);
                }
                else if (ipNouveauJoueur != null)
                {
                    messageRecu = communicationServeur.EcouterMessage(ipNouveauJoueur);
                    if (messageRecu != null)
                    {
                        if (messageRecu.Equals(codePartie))
                        {
                            communicationServeur.EnvoyerUnMessage("joueurAccepte:true", ipNouveauJoueur);
                            listeJoueurs.Add(new Player());
                            listeJoueurs[listeJoueurs.Count - 1].AdresseIP = ipNouveauJoueur;
                            listeJoueurs[listeJoueurs.Count - 1].Pseudo = ipNouveauJoueur;
                            listeJoueurs[listeJoueurs.Count - 1].Disponible = false;
                       
                            Invoke((DelegateAdd)AddUserCheckBox, ipNouveauJoueur);
                        }
                        else
                        {
                            communicationServeur.EnvoyerUnMessage("joueurAccepte:false", ipNouveauJoueur);
                            communicationServeur.DeconnecterClient(ipNouveauJoueur);
                        }
                        messageRecu = null;
                        ipNouveauJoueur = null;
                    }
                }

                /**
                 * Cette partie se charge d'écouter les messages envoyés par les joueurs (pseudo et disponibilitée)
                 */
                nbrJoueursPrets = 0;
                foreach (Player joueur in listeJoueurs)
                {
                    messageRecu = communicationServeur.EcouterMessage(joueur.AdresseIP);
                    if (messageRecu != null)
                    {
                        if (messageRecu.Contains("pseudo"))
                            joueur.Pseudo = messageRecu.Substring(messageRecu.IndexOf(":") + 1);
                        if (messageRecu.Contains("disponibilite"))
                        {
                            disponibiliteJoueur = messageRecu.Substring(messageRecu.IndexOf(":") + 1);
                            if (disponibiliteJoueur.Equals("pret"))
                                joueur.Disponible = true;
                            else
                                joueur.Disponible = false;
                        }
                    }
                    if(joueur.Disponible)     
                        nbrJoueursPrets++;

                    //Possibilité de try catch les Invoke si nécessaire
                    Invoke((DelegateRefresh)RefreshUserCheckBox, joueur);


                        

                }

                /**
                 * Cette partie vérifier si tous les joueurs sont prêt et lance un timer quand c'es le cas
                 * (et annule celui-ci si un des joueur passe son statut en "pas prêt")
                 */
                if (listeJoueurs.Count == nbrJoueursPrets && listeJoueurs.Count != 0)
                    tousLesJoueursSontPrets = true;
                else
                    tousLesJoueursSontPrets = false;
                 
                Invoke((DelegateDecompte)Decompte); 

            } 

        }

        /**
         * Ajoute un utilisateur à la check box
         */
        public void AddUserCheckBox(String ipNouveauJoueur)
        {
            checkedListBox1.Items.Add(ipNouveauJoueur);
        }

        /**
         * rafraichis la checkbox avec les paramètres de l'utilisateur
         */
        public void RefreshUserCheckBox(Player joueur)
        {
            checkedListBox1.Items[listeJoueurs.IndexOf(joueur)] = joueur.Pseudo;
            checkedListBox1.SetItemChecked(listeJoueurs.IndexOf(joueur), joueur.Disponible);
        }

        /**
         * Affiche un decompte de 5 secondes
         */
        public void Decompte()
        {
            if (tousLesJoueursSontPrets && !timer.Enabled)
            {
                this.timer = new Timer();
                this.textCounter.Text = "5";
                this.timer.Interval = 1000;
                this.timer.Tick += new EventHandler(UneSecondeEcouleeEvenement);
                this.timer.Start();
            }
            else if (!tousLesJoueursSontPrets && timer.Enabled)
            {
                timer.Stop();
                cptTicksTimer = 0;
                textCounter.Text = "";
            }
        }

        private void UneSecondeEcouleeEvenement(Object myObject, EventArgs myEventArgs)
        {
            cptTicksTimer++;
            if (cptTicksTimer > 5)
            {
                this.lancementPartie = true;
                //Il faut fermer d'abord le thread car celui-ci utilise des ressources du Thread
                //principale et crash donc car ces ressources sont détruites lors de l'appel de Close()

                foreach (Player joueur in listeJoueurs)
                    this.communicationServeur.EnvoyerUnMessage("demarragePartie:true", joueur.AdresseIP);

                this.timer.Stop();
                this.threadTest.Abort();
                this.Close();

            }
            textCounter.Text = (5 - cptTicksTimer).ToString();
        }


        public void AfficherCodePartie()
        {
            this.codePartieLabel.Text = "Tag : " + codePartie;
        }

    }
}
