﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Jeu
{
    static class Program
    {
        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            CommunicationServeur communicationServeur = new CommunicationServeur();
            List<Player> listeJoueurs = new List<Player>();

            Application.Run(new FormConnexion(communicationServeur, listeJoueurs));

            /*
            
            listeJoueurs.Add(new Player());
            listeJoueurs[0].AdresseIP = "192.168.1.100";
            listeJoueurs[0].Pseudo = "Roro";

            listeJoueurs.Add(new Player());
            listeJoueurs[1].AdresseIP = "192.168.1.101";
            listeJoueurs[1].Pseudo = "Pierre";

            listeJoueurs.Add(new Player());
            listeJoueurs[2].AdresseIP = "192.168.1.102";
            listeJoueurs[2].Pseudo = "Paul";

            listeJoueurs.Add(new Player());
            listeJoueurs[3].AdresseIP = "192.168.1.103";
            listeJoueurs[3].Pseudo = "Jacques";
            
            */

            Application.Run(new FormJeu(communicationServeur, listeJoueurs));
          
        }
    }
}
