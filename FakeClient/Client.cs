﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Client
{
    /* Cette classe permet de simuler un smartphone se connectant au Jeu.
     * Elle permet d'établir la connection puis d'envoyer des messages prédéfinis au serveur.
     * Elle n'affiche pas les messages reçus car cela n'est pas nécessaire pour les tests, le smartphone
     * n'ayant que le rôle de télécommande et n'ayant pour seul réaction au message reçu qu'un changement
     * d'interface.
     * Cette classe utilise les méthodes définit dans la classe CommunicationClient pour communiquer avec le serveur
     * et gérer la partie réseau.
     */

    public class Client
    {
        public static int Main(String[] args)
        {
            int choixUtilisateur = 0;
            CommunicationClient communicationClient = new CommunicationClient();

            Console.WriteLine("----------Client-----------");
            Console.WriteLine("Rentrez le code pour vous connecter...");
            communicationClient.ConnexionServeur("192.168.1.202", 11000);
            Thread.Sleep(500);
            communicationClient.EnvoyerUnMessage(Console.ReadLine());
            Thread.Sleep(500);
            communicationClient.EnvoyerUnMessage("pseudo:emulMobile");

            do
            {
                Console.WriteLine("----------Client-----------");
                Console.Clear();

                //Le menu permet de choisir parmis plusieurs en-têtes de messages pré-définis.
                Console.WriteLine("Type de message :");
                Console.WriteLine("1 - disponibilite");
                Console.WriteLine("2 - choixEntreeSalle");
                Console.WriteLine("3 - choixAiderJoueur");
                Console.WriteLine("4 - Direction");
                Console.WriteLine("0 - Quitter");

                choixUtilisateur = Int32.Parse(Console.ReadLine());
                //Il faut ensuite entrer soit même le contenu du message, en fonction de l'action souhaitée.
                Console.WriteLine("Entrer le contenu : ");
                String messageAEnvoye = Console.ReadLine();
                switch (choixUtilisateur)
                {
                    case 1:
                        communicationClient.EnvoyerUnMessage("disponibilite:" + messageAEnvoye);
                        break;
                    case 2:
                        communicationClient.EnvoyerUnMessage("choixEntreeSalle:" + messageAEnvoye);
                        break;
                    case 3:
                        communicationClient.EnvoyerUnMessage("choixAiderJoueur:" + messageAEnvoye);
                        break;
                    case 4:
                        communicationClient.EnvoyerUnMessage("choixDirection:" + messageAEnvoye);
                        break;
                }
                Console.WriteLine("Message envoyé");

            } while (choixUtilisateur != 0);

            return 0;
        }

    }
}
