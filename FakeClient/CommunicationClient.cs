﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;

namespace Client
{   

    /* Cette classe contient les méthodes nécessaires pour la communication réseau d'un client.
     * Elle utilise pour se faire un socket, configuré pour supporter le proctocole TCP.
     */
    class CommunicationClient
    {   
        private Socket socketClient;

        /* Créer le socket et le configure pour le protocole TCP */
        public CommunicationClient()
        {
            socketClient = new Socket(SocketType.Stream, ProtocolType.Tcp);
        }

        /* Permet au client d'établir une connection avec le serveur. Il lui faut pour cela l'adresse IP du serveur et le 
         * numéro de port à utiliser. */
        public void ConnexionServeur(String adresseIP, int port)
        {
            try
            {
                socketClient.Connect(adresseIP, port);
                Console.WriteLine("Connexion établie avec " + socketClient.RemoteEndPoint.ToString() + "\n");
            }
            catch (Exception e)
            {
                Console.WriteLine("ERREUR : " + e.ToString());
                Console.WriteLine("STACK TRACE : " + e.StackTrace);
            }
        }

        /* Permet la déconnection du client et la destruction de son socket, afin de libérer les ressources réseau utilisées et de 
         * permetrre l'établissement d'une nouvelle connection si nécessaire. */
        public void DeconnexionServeur()
        {
            try
            {
                // Release the socket.    
                socketClient.Shutdown(SocketShutdown.Both);
                socketClient.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("ERREUR : " + e.ToString());
                Console.WriteLine("STACK TRACE : " + e.StackTrace);
            }
        }
        
        /* Cette méthode permet d'envoyer un message au serveur. Pour cela le message est préalablement convertit 
         * en tableau d'octets */
        public void EnvoyerUnMessage(String message)
        {
            byte[] msg = Encoding.ASCII.GetBytes(message);
            try
            {
                socketClient.Send(msg);
            }
            catch (Exception e)
            {
                Console.WriteLine("ERREUR : " + e.ToString());
                Console.WriteLine("STACK TRACE : " + e.StackTrace);
            }
        }

        /* Cette méthode permet d'écouter si le serveur envoit un message au client et le cas échéant, de recevoir celui-ci.
         * Le message est reçu sous la forme d'un tableau d'octets et doit être re-convertit en string.
         * Ici, la méthode est bloquante et son débloquage (conséquence de l'appel à Receive()) n'a lieu que lors de la reception
         * d'un message quelqu'il soit.
         */
        public Boolean RecevoirMessage(ref String message)
        {
            //octet = byte et byte = entier non signé sur 8 bits
            try
            {
                int nbrOctetsRecus;
                byte[] msgRecuEnBinaire = new byte[1024];
                nbrOctetsRecus = socketClient.Receive(msgRecuEnBinaire);
                message = Encoding.ASCII.GetString(msgRecuEnBinaire, 0, nbrOctetsRecus);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("ERREUR : " + e.ToString());
                Console.WriteLine("STACK TRACE : " + e.StackTrace);
                return false;
            }
        }


    }
}
