﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterfaceAPIJeu
{   
    /* Cette classe décrit les données et la strucutre d'un objet Monstre tel qu'il est 
     * stockée en BDD. Son contenu est le même que la classe homonyme dans le projet APIJEu
     */
    public class Monstre
    {   
        public long Id { get; set; }
        public string Nom { get; set; }
        public int Level { get; set; }
        public string FilePathImage { get; set; }
        public string Description { get; set; }

    }
}
