﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace InterfaceAPIJeu
{
    /* Cette classe est le controlleur de la vue XAML du même nom.
     * Elle permet d'afficher, ajouter et supprimer les monstres de la BDD (via des reqêtes à l'API)
     */
    public partial class MainWindow : Window
    {
        private String filePathImage;
        private List<Monstre> listeMonstres;
   
        public MainWindow()
        {
            InitializeComponent();
            filePathImage = null;
        }

        /* Méthode appelée lors du click sur le bouton "Ajouter"
         * Cette méthode vérifie que les champs du formulaire d'ajout d'un nouveau Monstre ne sont pas vides, puis enregistre le nouveau monstre en BDD
         * tout en copiant l'image du nouveau monstre dans le dossier Ressources/lvl du projet Jeu */
        private void buttonAjouterMonstre_Click(object sender, RoutedEventArgs e)
        {
            Monstre m;
            int nbrDeFichiers;
            String cheminAccesMonstreEnregistre;

            if (!String.IsNullOrEmpty(nomMonstre.Text)  && !String.IsNullOrEmpty(lvlMonstre.Text)  && !String.IsNullOrEmpty(filePathImage) && !String.IsNullOrEmpty(descriptionMonstre.Text))
            {
                m = new Monstre();
                m.Nom = nomMonstre.Text;
                m.Level = Int32.Parse(lvlMonstre.Text);
                cheminAccesMonstreEnregistre = "C:/Users/rlv78/Workspace_VisualStudio/projetcsharp/Jeu/Resources/monstres/lvl" + m.Level;
                nbrDeFichiers = Directory.GetFiles(cheminAccesMonstreEnregistre, "*.png", SearchOption.TopDirectoryOnly).Length;
                cheminAccesMonstreEnregistre += "/monstre" + nbrDeFichiers + ".png";
                File.Copy(filePathImage, cheminAccesMonstreEnregistre, true);
                m.FilePathImage = cheminAccesMonstreEnregistre;
                m.Description = descriptionMonstre.Text;

                _ = API.Instance.AjoutMonstreAsync(m);
                nomMonstre.Text = "";
                lvlMonstre.Text = "";
                filePathImage = null;
                imageMonstre.Visibility = Visibility.Collapsed;
                texteAjouterImage.Visibility = Visibility.Visible;
                descriptionMonstre.Text = "";
            }
            else
            {
                MessageBox.Show(
                            "Tous les champs doivent être remplis !",
                            "Champs non remplis",
                            MessageBoxButton.OK,
                            MessageBoxImage.Warning);
            }

        }

        /* Méthode appelée lors du click sur le bouton "Afficher". Elle requête l'API et affiche tous les monstres enregistrés en BDD retournés
         * par l'API.
         */
        private void buttonAfficherMonstres_Click(object sender, RoutedEventArgs e)
        {
            listeMonstres = API.Instance.GetListeMonstresAsync().Result;

            Border borderEncartMonstre;
            StackPanel panelEncartMonstre;
            Border borderAffichageImage;
            StackPanel panelAffichageImage;
            Image imageAffichageMonstre;
            StackPanel panelAffichageCaracteristiques;
            TextBlock encartNomMonstre;
            TextBlock encartLvlMonstre;
            TextBlock encartDescriptionMonstre;

            panelListeMonstres.Children.Clear();

            foreach (Monstre m in listeMonstres)
            {
                //Encart Monstre
                borderEncartMonstre = new Border();
                borderEncartMonstre.Background = new SolidColorBrush(Color.FromRgb(52, 152, 219));
                borderEncartMonstre.Margin = new Thickness(10, 5, 10, 5);
                borderEncartMonstre.Padding = new Thickness(5);
                borderEncartMonstre.BorderThickness = new Thickness(1);
                borderEncartMonstre.BorderBrush = new SolidColorBrush(Color.FromRgb(0, 0, 0));
                borderEncartMonstre.MouseLeftButtonDown += new MouseButtonEventHandler(panelEncartMonstre_MouseLeftButtonDown);
                borderEncartMonstre.DataContext = m.Id;
                panelEncartMonstre = new StackPanel();
                panelEncartMonstre.Orientation = Orientation.Horizontal;

                //Image
                borderAffichageImage = new Border();
                borderAffichageImage.BorderThickness = new Thickness(1);
                borderAffichageImage.BorderBrush = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                borderAffichageImage.Margin = new Thickness(0, 0, 10, 0);
                panelAffichageImage = new StackPanel();
                panelAffichageImage.VerticalAlignment = VerticalAlignment.Center;
                imageAffichageMonstre = new Image();
                imageAffichageMonstre.Width = 50;
                imageAffichageMonstre.Height = 50;
                imageAffichageMonstre.VerticalAlignment = VerticalAlignment.Center;

                //Caractéritiques du monstre
                panelAffichageCaracteristiques = new StackPanel();
                encartNomMonstre = new TextBlock();
                encartNomMonstre.FontSize = 8;
                encartNomMonstre.FontWeight = FontWeights.Bold;
                encartLvlMonstre = new TextBlock();
                encartLvlMonstre.FontSize = 8;
                encartDescriptionMonstre = new TextBlock();
                encartDescriptionMonstre.FontSize = 8;
                encartDescriptionMonstre.MaxWidth = 150;
                encartDescriptionMonstre.TextWrapping = TextWrapping.Wrap;

                //Ajout des panel et des controles les uns dans les autres
                borderEncartMonstre.Child = panelEncartMonstre;
                panelEncartMonstre.Children.Add(borderAffichageImage);
                panelEncartMonstre.Children.Add(panelAffichageCaracteristiques);

                borderAffichageImage.Child = panelAffichageImage;
                panelAffichageImage.Children.Add(imageAffichageMonstre);

                panelAffichageCaracteristiques.Children.Add(encartNomMonstre);
                panelAffichageCaracteristiques.Children.Add(encartLvlMonstre);
                panelAffichageCaracteristiques.Children.Add(encartDescriptionMonstre);

                panelListeMonstres.Children.Add(borderEncartMonstre);

                //remplissage avec les données
                encartNomMonstre.Text = m.Nom;
                encartLvlMonstre.Text = "lvl " + m.Level.ToString();
                BitmapImage bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad; //Permet de de fermer le fichier une fois l'image chargée
                bitmapImage.UriSource = new Uri(m.FilePathImage, UriKind.Absolute);
                bitmapImage.EndInit();
                imageAffichageMonstre.Source = bitmapImage;

                encartDescriptionMonstre.Text = m.Description;
            }

        }

        /* Méthode est appelée lors du click sur l'encart contenant l'image du monstre à ajouter. 
         * Elle ouvre une OpenFileDialog permettant de sélectionner l'image (obligatoirement au format .png) à ajouter au monstre */
        private void panelImage_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.DefaultExt = ".png"; // Default file extension
            dlg.Filter = "Images vectorielles (.png)|*.png"; // Filter files by extension


            Nullable<bool> result = dlg.ShowDialog(); // Show open file dialog box
            // Process open file dialog box results
            if (result == true)
            {
                // Open document
                filePathImage = dlg.FileName;
                imageMonstre.Source = new BitmapImage(new Uri(filePathImage));
                imageMonstre.Visibility = Visibility.Visible;
                texteAjouterImage.Visibility = Visibility.Collapsed;
            }
        }

        /* Méthode appelée lors du click sur un des encart présentant les caractéristique des monstres dans la liste déroulante de monstre.
         * Un fenêtre de dialogue demande alors de confirmer la suppresion et le cas échéant le monstre est supprimer.
         * L'affichage de la liste des monstres est ensuite actualisé après avoir attendu que la requête de suppresion ait bien atteinte la BDD.
         * L'actualisation de l'affichage de la liste des monstres se fait via l'appel de la méthode buttonAfficherMonstres_Click()
         * L'id du monstre à supprimer est passé via la fonction DataContext du StackPanel contenant le monstre cliqué.
         */
        private void panelEncartMonstre_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Monstre monstreASupprimer = new Monstre();
            foreach (Monstre m in listeMonstres)
                if (m.Id == Int32.Parse(((Border)sender).DataContext.ToString()))
                    monstreASupprimer = m;

            MessageBoxResult result = MessageBox.Show(
                "Etes vous sûr de vouloir supprimer le monstre " + monstreASupprimer.Nom,
                "Suppression",
                MessageBoxButton.OKCancel,
                MessageBoxImage.Warning);

            if (result == MessageBoxResult.OK)
            {
                int idMonstreASupprimer = (int)monstreASupprimer.Id;
                _ = API.Instance.SupprimerMonstreAsync(idMonstreASupprimer);
                //Permet au monstre de correctement se supprimer avant de ré-afficher
                Thread.Sleep(500);
                this.buttonAfficherMonstres_Click(this.buttonAfficherMonstres, new RoutedEventArgs(ButtonBase.ClickEvent));
                File.Delete(monstreASupprimer.FilePathImage); //marche parce qu'on a fermé l'image après l'avoir lu (BitmapImage)
            }

        }
    }
}
