﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace InterfaceAPIJeu
{   
    /* Cette classe permet de réaliser les requêtes en HTPP / JSON vers l'API et de convertir directement le résultat
     *  en objets.
     * Le nom des méthodes est assez explicite et cette classe est très fortement inspirée de celle envoyée par le 
     * professeur. 
     */
    public sealed class API
    {
        private static readonly HttpClient client = new HttpClient();
        public static API Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new API();
                    }
                    return instance;
                }
            }
        }

        private API()
        {
            client.BaseAddress = new Uri("http://localhost:52276/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }

        private static readonly object padlock = new object();
        private static API instance = null;
        

        public async Task<List<Monstre>> GetListeMonstresAsync()
        {
            List<Monstre> listeMonstres = new List<Monstre>();
            HttpResponseMessage response = client.GetAsync("api/monstres").Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                listeMonstres = JsonConvert.DeserializeObject<List<Monstre>>(resp);
            }
            return listeMonstres;
        }

        public async Task<Monstre> GetMonstreAsync(int? id)
        {
            Monstre monstre = null;
            HttpResponseMessage response = client.GetAsync("api/monstres/" + id).Result;
            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsStringAsync();
                monstre = JsonConvert.DeserializeObject<Monstre>(resp);
            }
            return monstre;
        }

        public async Task<Uri> AjoutMonstreAsync(Monstre monstre)
        {
            try
            {
                HttpResponseMessage response = await client.PostAsJsonAsync("api/monstres", monstre);
                response.EnsureSuccessStatusCode();
                return response.Headers.Location;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        public async Task<Uri> ModifierMonstreAsync(Monstre monstre)
        {
            try
            {
                HttpResponseMessage response = await client.PutAsJsonAsync("api/monstres/" + monstre.Id, monstre);
                response.EnsureSuccessStatusCode();
                return response.Headers.Location;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        public async Task<Uri> SupprimerMonstreAsync(int id)
        {
            try
            {
                HttpResponseMessage response = await client.DeleteAsync("api/monstres/" + id);
                response.EnsureSuccessStatusCode();
                return response.Headers.Location;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }
    }
}
