﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIJeu.Models
{   
    /* Classe déclarant la structure et le données d'un Monstre.
     * Un monstre est un objet représentant un monstre dans le jeu.
     * Le FilePathImage représente le chemin d'accès à la photo du monstre.
     * Cette photo est stockée dans le répertoire Ressources/lvl du projet Jeu
     */
    public class Monstre
    {   
        public long Id { get; set; }
        public string Nom { get; set; }
        public int Level { get; set; }
        public string FilePathImage { get; set; }
        public string Description { get; set; }

    }
}
