﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace APIJeu.Models
{   
    /**
     * Classe déclarant le context de l'api.
     * 
     */
    public class APIJeuContext : DbContext
    {
        public APIJeuContext(DbContextOptions<APIJeuContext> options): base(options)
        {
        }
        public DbSet<Monstre> Monstres { get; set; }
    }
}
