﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIJeu.Models;


namespace APIJeu.Controllers
{

    /**
    * Controlleur du monstre, généré à l'aide de Visual Studio.
    * Permet d'interagir avec la base de données.
    * 
    */

    [Route("api/[controller]")]
    [ApiController]
    public class MonstresController : ControllerBase
    {
        private readonly APIJeuContext _context;

        public MonstresController(APIJeuContext context)
        {
            _context = context;
        }

        // GET: api/Monstres
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Monstre>>> GetMonstres()
        {
            return await _context.Monstres.ToListAsync();
        }

        // GET: api/Monstres/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Monstre>> GetMonstre(long id)
        {
            var monstre = await _context.Monstres.FindAsync(id);

            if (monstre == null)
            {
                return NotFound();
            }

            return monstre;
        }

        // PUT: api/Monstres/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMonstre(long id, Monstre monstre)
        {
            if (id != monstre.Id)
            {
                return BadRequest();
            }

            _context.Entry(monstre).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MonstreExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Monstres
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Monstre>> PostMonstre(Monstre monstre)
        {
            _context.Monstres.Add(monstre);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMonstre", new { id = monstre.Id }, monstre);
        }

        // DELETE: api/Monstres/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Monstre>> DeleteMonstre(long id)
        {
            var monstre = await _context.Monstres.FindAsync(id);
            if (monstre == null)
            {
                return NotFound();
            }

            _context.Monstres.Remove(monstre);
            await _context.SaveChangesAsync();

            return monstre;
        }

        private bool MonstreExists(long id)
        {
            return _context.Monstres.Any(e => e.Id == id);
        }
    }
}
